$(function () {
    var globalObj = {
        currentArtistPage: 1,
        currentAlbumPage: 1,
        artist: null,
        album: null,
        track: null,
        showLoading: function () {
            $('.loading').show();
            $('.container').hide();
        },
        hideLoading: function () {
            $('.loading').hide();
            $('.container').show();
        },
        showArtistList: function () {
            $('.searchArtistContainer').show();
            $('.searchAlbumContainer').hide();
        },
        showAlbumList: function () {
            $('.searchAlbumContainer').show();
            $('.searchArtistContainer').hide();
        },
        handleClickOnSearchArtistButton: function () {
            var that = this;
            $('#searchArtist').click(function () {
                that.artist = $('#artistname').val();
                that.showLoading();
                that.getArtistSearchData();
            });
        },
        handleClickOnViewAblumsButton: function () {
            var that = this;
            $('body').on('click', '.viewAlbumsButton', function () {
                that.showAlbumList();
                that.showLoading();
                that.album = $(this)[0].id;
                that.getSingleArtistData();
                that.getAlbumSearchData();
            })
        },
        handleClickOnViewTracksButton: function () {
            var that = this;
            $('body').on('click', '.viewTracksButton', function () {
                that.track = $(this)[0].id;
                that.getSingleAlbumData();
                that.getTrackSearchData();
            })
        },
        handleClickOnNextArtistPage: function () {
            var that = this;
            $('#nextArtist').click(function () {
                that.currentArtistPage = that.currentArtistPage + 1;
                $('.artistPage').html(that.currentArtistPage);
                that.showLoading();
                that.getArtistSearchData();
            });
        },
        handleClickOnNextAlbumPage: function () {
            var that = this;
            $('#nextAlbum').click(function () {
                that.currentAlbumPage = that.currentAlbumPage + 1;
                $('.albumPage').html(that.currentAlbumPage);
                that.showLoading();
                that.getAlbumSearchData();
            });
        },
        handleClickOnPrevAlbumPage: function () {
            var that = this;
            $('#prevAlbum').click(function () {
                that.currentAlbumPage = that.currentAlbumPage - 1;
                if (that.currentAlbumPage < 1) that.currentAlbumPage = 1;
                $('.albumPage').html(that.currentAlbumPage);
                that.showLoading();
                that.getAlbumSearchData();
            });
        },
        handleClickOnPrevArtistPage: function () {
            var that = this;
            $('#prevArtist').click(function () {
                that.currentArtistPage = that.currentArtistPage - 1;
                if (that.currentArtistPage < 1) that.currentArtistPage = 1;
                $('.artistPage').html(that.currentArtistPage);
                that.showLoading();
                that.getArtistSearchData();
            });
        },
        handleClickOnBack: function () {
            var that = this;
            $('#backToArtist').click(function () {
                that.showArtistList();
            })
        },
        regiterClickHandlers: function () {
            this.handleClickOnSearchArtistButton();
            this.handleClickOnViewAblumsButton();
            this.handleClickOnNextAlbumPage();
            this.handleClickOnNextArtistPage();
            this.handleClickOnPrevAlbumPage();
            this.handleClickOnPrevArtistPage();
            this.handleClickOnViewTracksButton();
            this.handleClickOnBack();
        },
        appendArtistToList: function (data) {
            $('.artistCollection').html('');
            var ArtistString = '';
            $.each(data.artists.items, function (i, val) {
                ArtistString = ArtistString + '<li class="collection-item avatar">';
                if (val.images.length != 0)
                    ArtistString = ArtistString + '<img src="' + val.images[0].url + '" alt="" class="circle">';

                ArtistString = ArtistString + '<span class="title">' + val.name + '</span>' +
                    '<p>' + val.followers.total + ' followers' +
                    '</p><a class="waves-effect waves-light btn secondary-content viewAlbumsButton" id="' + val.id + '">View Albums</a>' +
                    '</li>';
            })
            $('.artistCollection').append(ArtistString);
            this.hideLoading();
        },
        appendAlbumToList: function (data) {
            $('.albumCollection').html('');
            var ArtistString = '';
            $.each(data.items, function (i, val) {
                ArtistString = ArtistString + '<li class="collection-item avatar">';
                if (val.images.length != 0)
                    ArtistString = ArtistString + '<img src="' + val.images[0].url + '" alt="" class="circle">';

                ArtistString = ArtistString + '<span class="title">' + val.name + '</span>' +
                    '<p>' + val.type + ' ' +
                    '</p><a  href="#modal1" class="waves-effect waves-light btn secondary-content viewTracksButton" id="' + val.id + '">View Tracks</a>' +
                    '</li>';
            })
            $('.albumCollection').append(ArtistString);
            this.hideLoading();
        },
        appendTrackToList: function (data) {
            $('.trackCollection').html('');
            var trackString = '';
            $.each(data.items, function (i, val) {
                trackString = trackString + '<li class="collection-item"><span>' + val.track_number + ' ' + val.name +
                    ' ' + (val.duration_ms) / 1000 + 's</span></li>'
            })
            $('.trackCollection').append(trackString);
            this.hideLoading();
        },
        getArtistSearchData: function () {
            var that = this;
            $.get('https://api.spotify.com/v1/search?q=' + this.artist + '&type=artist&offset=' + (this.currentArtistPage - 1) * 20, function (data) {
                that.appendArtistToList(data);
            })
        },
        getSingleArtistData: function () {
            var that = this;
            $.get('https://api.spotify.com/v1/artists/' + this.album, function (data) {
                var appendAtistImage = '';
                if (data.images.length > 0)
                    appendAtistImage = '<img class="responsive-img" src="' + data.images[0].url + '"/>'
                $('.artistImage').html(appendAtistImage);
                var appendArtistdata = '<p>' + data.name + '<br/><p>Followers: ' + data.followers.total + '</p>';
                $('.artistDetail').html(appendArtistdata);
                //that.appendAlbumToList(data);
            })
        },
        getAlbumSearchData: function () {
            var that = this;
            $.get('https://api.spotify.com/v1/artists/' + this.album + '/albums?offset=' + (this.currentAlbumPage - 1) * 20, function (data) {
                that.appendAlbumToList(data);
            })
        },
        getTrackSearchData: function () {
            var that = this;
            $.get('https://api.spotify.com/v1/albums/' + this.track + '/tracks', function (data) {
                that.appendTrackToList(data);
            })
        },
        getSingleAlbumData: function () {
            var that = this;
            $.get('https://api.spotify.com/v1/albums/' + this.track, function (data) {
                console.log(data);
                var albumStr = '<p>' + data.name + ' ' + data.release_date + '</p>';
                $('.albumDetailSingle').append(albumStr);
            })
        },
        init: function () {
            this.showArtistList();
            $('.modal').modal();
            this.hideLoading();
            this.regiterClickHandlers();
        }
    }
    globalObj.init();
})